# frozen_string_literal: true

require_relative 'base_policy'
require_relative '../entity_builders/issue_builder'

module Gitlab
  module Triage
    module Policies
      class SummaryPolicy < BasePolicy
        # Build an issue from several rules policies
        def build_issue
          action = actions[:summarize]
          issues = resources.build_issues do |inner_policy_spec, inner_resources|
            Policies::RulePolicy.new(
              type, inner_policy_spec, inner_resources, network)
              .build_issue
          end

          EntityBuilders::IssueBuilder.new(
            type: type,
            action: action,
            resources: issues.select(&:any_resources?),
            network: network,
            separator: "\n\n")
        end
      end
    end
  end
end
