# frozen_string_literal: true

module Gitlab
  module Triage
    module PoliciesResources
      class RuleResources
        attr_reader :resources

        def initialize(new_resources)
          @resources = new_resources
        end

        def each
          resources.each do |resource|
            yield(resource)
          end
        end
      end
    end
  end
end
